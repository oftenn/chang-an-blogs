## setup

- 新的 option, 所有的组合 API 函数都在此使用, 只在初始化时执行一次
- 函数如果返回对象, 对象中的属性或方法, 模板中可以直接使用

```html
<template>
  <h2>{{count}}</h2>
  <hr />
  <button @click="update">更新</button>
</template>

<script>
  import { ref } from "vue";
  export default {
    /* 在Vue3中依然可以使用data和methods配置, 但建议使用其新语法实现 */
    // data () {
    //   return {
    //     count: 0
    //   }
    // },
    // methods: {
    //   update () {
    //     this.count++
    //   }
    // }

    /* 使用vue3的composition API */
    setup() {
      // 定义响应式数据 ref对象
      const count = ref(1);
      console.log(count);

      // 更新响应式数据的函数
      function update() {
        // alert('update')
        count.value = count.value + 1;
      }

      return {
        count,
        update,
      };
    },
  };
</script>
```

### ref

- 作用: 定义一个数据的响应式
- 语法: const xxx = ref(initValue):
  - 创建一个包含响应式数据的引用(reference)对象
  - js 中操作数据: xxx.value
  - 模板中操作数据: 不需要.value
- 一般用来定义一个基本类型的响应式数据

```html
<template>
  <h2>{{ count }}</h2>
  <hr />
  <button @click="update">更新</button>
</template>

<script setup>
  import { ref } from "vue";
  // 定义响应式数据 ref对象
  const count = ref(1);
  console.log(count);

  // 更新响应式数据的函数
  function update() {
    // alert('update')
    count.value = count.value + 1;
  }
</script>
```

## reactive

- 作用: 定义多个数据的响应式
- const proxy = reactive(obj): 接收一个普通对象然后返回该普通对象的响应式代理器对象
- 响应式转换是“深层的”：会影响对象内部所有嵌套的属性
- 内部基于 ES6 的 Proxy 实现，通过代理对象操作源对象内部数据都是响应式的

```html
<template>
  <h2>name:{{ state.name }}</h2>
  <h2>age:{{ state.age }}</h2>

  <h2>wife:{{ state.wife }}</h2>

  <hr />
  <button @click="update">更新</button>
</template>

<script setup>
  import { reactive, ref } from "vue";
  // 定义响应式数据 ref对象
  const state = reactive({
    name: "常安",
    age: 25,
    wife: {
      name: "常安",
      age: 22,
    },
  });
  console.log(state);

  // 更新响应式数据的函数
  const update = () => {
    state.name = state.name + "王";
    state.age = state.age + 1;
    state.wife.name = state.wife.name + "李";
    state.wife.age = state.wife.age + 2;
  };
</script>
<style scoped></style>
```

## 比较 Vue2 与 Vue3 的响应式(重要)

### vue2 的响应式

- 核心
  - 对象: 通过 defineProperty 对对象的已有属性值的读取和修改进行劫持(监视/拦截)
  - 数组: 通过重写数组更新数组一系列更新元素的方法来实现元素修改的劫持

```js
Object.defineProperty(data, "count", {
  get() {},
  set() {},
});
```

- 问题
  - 对象直接新添加的属性或删除已有属性, 界面不会自动更新
  - 直接通过下标替换元素或更新 length, 界面不会自动更新 arr[1] = {}

### Vue3 的响应式

- 核心
  - 通过 Proxy(代理): 拦截对 data 任意属性的任意(13 种)操作, 包括属性值的读写, 属性的添加, 属性的删除等...
  - 通过 Reflect(反射): 动态对被代理对象的相应属性进行特定的操作

```js
// 参数一 target 要使用 Proxy 包装的目标对象（可以是任何类型的对象，包括原生数组，函数，甚至另一个代理）。
// 参数二 handler 一个通常以函数作为属性的对象，各属性中的函数分别定义了在执行各种操作时代理 p 的行为。
// handler.get() 方法用于拦截对象的读取属性操作。
// handler.set() 方法是设置属性值操作的捕获器。
// handler.defineProperty() 用于拦截对象的 Object.defineProperty() 操作。
// target目标对象。 property待检索其描述的属性名。 descriptor待定义或修改的属性的描述符。
new Proxy(data, {
  // 拦截读取属性值
  get(target, prop) {
    return Reflect.get(target, prop);
  },
  // 拦截设置属性值或添加新属性
  set(target, prop, value) {
    return Reflect.set(target, prop, value);
  },
  // 拦截删除属性
  deleteProperty(target, prop) {
    return Reflect.deleteProperty(target, prop);
  },
});

proxy.name = "tom";
```

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Proxy 与 Reflect</title>
  </head>
  <body>
    <script>
      const user = {
        name: "常安",
        age: 18,
      };

      /* 
    proxyUser是代理对象, user是被代理对象
    后面所有的操作都是通过代理对象来操作被代理对象内部属性
    */
      const proxyUser = new Proxy(user, {
        get(target, prop) {
          console.log("劫持get()", prop);
          return Reflect.get(target, prop);
        },

        set(target, prop, val) {
          console.log("劫持set()", prop, val);
          return Reflect.set(target, prop, val); // (2)
        },

        deleteProperty(target, prop) {
          console.log("劫持delete属性", prop);
          return Reflect.deleteProperty(target, prop);
        },
      });
      // // 读取属性值
      console.log(proxyUser === user);
      console.log(proxyUser.name, proxyUser.age);
      // // 设置属性值
      proxyUser.name = "bob";
      proxyUser.age = 13;
      console.log(user);
      // // 添加属性
      proxyUser.sex = "男";
      console.log(user);
      // // 删除属性
      delete proxyUser.sex;
      console.log(user);
    </script>
  </body>
</html>
```

## setup 细节

### setup 执行的时机

- 在 beforeCreate 之前执行(一次), 此时组件对象还没有创建
- this 是 undefined, 不能通过 this 来访问 data/computed/methods / props
- 其实所有的 composition API 相关回调函数中也都不可以

**App 组件**

```vue
<template>
  <h2>App</h2>
  <p>msg: {{ msg }}</p>
  <chlid :msg="msg" />
</template>

<script lang="ts">
import { reactive, ref } from "vue";
import chlid from "./components/chlid.vue";

export default {
  components: {
    chlid,
  },
  setup() {
    const msg = ref("abc");
    return {
      msg, //setup返回值
    };
  },
};
</script>
```

**chlid 子组件**

```vue
<template>
  <div>
    <h2>子组件</h2>
    <h3>msg: {{ msg }}</h3>
  </div>
</template>
<script lang="ts">
import { ref, defineComponent } from "vue";
export default defineComponent({
  name: "child",
  props: ["msg"],
  beforeCreate() {
    console.log("beforeCreate", this); // 在执行 beforeCreate Proxy {…}
  },
  setup(props, { attrs, emit, slots }) {
    console.log("setup", this); //先执行 setup undefined

    return {};
  },
});
</script>
```

### setup 的返回值

- 一般都返回一个对象: 为模板提供数据, 也就是模板中可以直接使用此对象中的所有属性/方法
- 返回对象中的属性会与 data 函数返回对象的属性合并成为组件对象的属性
- 返回对象中的方法会与 methods 中的方法合并成功组件对象的方法
- 如果有重名, setup 优先

* 注意:
  - 一般不要混合使用: methods 中可以访问 setup 提供的属性和方法, 但在 setup 方法中不能访问 data 和 methods
  - setup 不能是一个 async 函数: 因为返回值不再是 return 的对象, 而是 promise, 模板看不到 return 对象中的属性数据

### setup 的参数

- setup(props, context) / setup(props, {attrs, slots, emit})
- props: 包含 props 配置声明且传入了的所有属性的对象
- attrs: 包含没有在 props 配置中声明的属性的对象, 相当于 this.$attrs
- slots: 包含所有传入的插槽内容的对象, 相当于 this.$slots
- emit: 用来分发自定义事件的函数, 相当于 this.$emit

**App 组件**

```vue
<template>
  <h2>App</h2>
  <p>msg: {{ msg }}</p>
  <button @click="fn('--')">更新</button>
  <chlid :msg="msg" msg2="子组件没有在props中注册的数据" @fn="fn" />
</template>

<script lang="ts">
import { reactive, ref } from "vue";
import chlid from "./components/chlid.vue";

export default {
  components: {
    chlid,
  },
  setup() {
    const msg = ref("abc");
    function fn(content) {
      msg.value += content;
    }
    return {
      msg,
      fn,
    };
  },
};
</script>
```

**chlid 子组件**

```vue
<template>
  <div>
    <h3>{{ n }}</h3>
    <h3>{{ m }}</h3>

    <h3>msg: {{ msg }}</h3>
    <h3>msg2: {{ $attrs.msg2 }}</h3>

    <slot name="xxx"></slot>

    <button @click="update">更新</button>
  </div>
</template>
<script lang="ts">
import { ref, defineComponent } from "vue";
export default defineComponent({
  name: "child",
  props: ["msg"],
  emits: ["fn"], // 可选的, 声明了更利于程序员阅读, 且可以对分发的事件数据进行校验
  beforeCreate() {
    console.log("beforeCreate", this); // 在执行 beforeCreate Proxy {…}
  },
  setup(props, { attrs, emit, slots }) {
    console.log(props.msg, attrs.msg2, slots, emit); //setup 参数
    const m = ref(2);
    const n = ref(3);
    console.log("setup", this); //先执行 setup undefined
    console.log(emit);
    function update() {
      m.value += 2;
      n.value += 2;
      emit("fn", "++");
    }
    return {
      m,
      n,
      update,
    };
  },
});
</script>
```

### setup 语法糖

直接在 script 标签中添加 setup 属性就可以直接使用 setup 语法糖了。使用 setup 语法糖后：

- 不用写 setup 函数
- 组件只需要引入不需要注册
- 属性和方法也不需要再返回，可以直接在 template 模板中使用。

**App 组件**

```vue
<template>
  <div class="wrapper">
    <h2>App组件</h2>
    <p>msg:{{ msg }}</p>
    <chlid />
  </div>
</template>

<script setup lang="ts">
import chlid from "./components/chlid.vue";
import { ref } from "vue";
let msg = ref("abc");
</script>

<style scoped></style>
```

**chlid 子组件**

```vue
<template>
  <div class="wrapper">我是子组件</div>
</template>

<script setup lang="ts"></script>

<style scoped></style>
```

### setup 语法糖中新增的 api

- defineProps：子组件接收父组件中传来的 props

  **父组件**

```vue
<template>
  <div class="wrapper">
    <h2>App组件</h2>
    <p>msg:{{ msg }}</p>
    <chlid :msg="msg" />
  </div>
</template>

<script setup lang="ts">
import chlid from "./components/chlid.vue";
import { ref } from "vue";
let msg = ref("父组件的数据abc");
</script>
```

**子组件 chlid**

```vue
<template>
  <div class="wrapper">
    <h2>子组件渲染父组件传过来的数据</h2>
    {{ msg }}
  </div>
</template>
<script setup lang="ts">
let props = defineProps({
  msg: {
    type: String,
    default: "123",
  },
});
console.log(props.msg); //父组件的数据abc
</script>

<style scoped></style>
```

- defineEmits：子组件调用父组件中的方法

  **父组件**

```vue
<template>
  <!-- 父组件自定义事件接收子组件分发的事件 -->
  <chlid @addNumb="func" :numb="numb"></chlid>
</template>
<script lang="ts" setup>
import { ref } from "vue";
import chlid from "./components/chlid.vue";
const numb = ref(0);
let func = () => {
  numb.value++;
};
</script>
```

**子组件 chlid**

```vue
<template>
  <div>{{ numb }}</div>
  <!-- 子组件的方法 -->
  <button @click="onClickButton">数值加1</button>
</template>
<script lang="ts" setup>
let props = defineProps({
  numb: {
    type: Number,
    default: NaN,
  },
});
console.log(props.numb);
const emit = defineEmits(["addNumb"]);
const onClickButton = () => {
  //emit(父组件中的自定义方法,参数一,参数二,...)
  emit("addNumb");
};
</script>
```

- defineExpose：子组件暴露属性，可以在父组件中拿到

**子组件 chlid**

```vue
<template>
  <div>子组件中的值{{ numb }}</div>
  <button @click="onClickButton">数值加1</button>
</template>
<script lang="ts" setup>
import { ref, defineExpose } from "vue";
let numb = ref(0);
function onClickButton() {
  numb.value++;
}
//暴露出子组件中的属性
defineExpose({
  numb,
});
</script>
```

**父组件**

```vue
<template>
  <chlid ref="myComponent"></chlid>
  <button @click="onClickButton">获取子组件中暴露的值</button>
</template>
<script lang="ts" setup>
import { ref, onMounted } from "vue";
import chlid from "./components/chlid.vue";
//注册ref，获取组件
const myComponent = ref();
function onClickButton() {
  //在组件的value属性中获取暴露的值
  console.log(myComponent.value.numb); //0
}
//注意：在生命周期中使用或事件中使用都可以获取到值，
//但在setup中立即使用为undefined
// console.log(myComponent.value.numb)  //undefined
const init = () => {
  // console.log(myComponent.value.numb)  //undefined
};
init();
onMounted(() => {
  console.log(myComponent.value.numb); //0
});
</script>
```

## reactive 与 ref-细节

- 是 Vue3 的 composition API 中 2 个最重要的响应式 API
- ref 用来处理基本类型数据, reactive 用来处理对象(递归深度响应式)
- 如果用 ref 对象/数组, 内部会自动将对象/数组转换为 reactive 的代理对象
- ref 内部: 通过给 value 属性添加 getter/setter 来实现对数据的劫持
- reactive 内部: 通过使用 Proxy 来实现对对象内部所有数据的劫持, 并通过 Reflect 操作对象内部数据
- ref 的数据操作: 在 js 中要.value, 在模板中不需要(内部解析模板时会自动添加.value)

```vue
<template>
  <div class="wrapper">
    <h3>{{ msg }}</h3>
    <h3>{{ msg1 }}</h3>
    <h3>{{ msg3 }}</h3>
    <button @click="update">更新</button>
  </div>
</template>

<script setup lang="ts">
import { ref, reactive } from "vue";
let msg = ref("abc");
let msg1 = reactive({ name: "常安", city: { name: "长安" } });
// 使用ref处理对象  ==> 对象会被自动reactive为proxy对象
const msg3 = ref({ name: "常安", city: { name: "长安" } });
console.log(msg3.value.city); // 也是一个proxy对象
function update() {
  msg.value += "--";
  msg1.name += "++";
  msg1.city.name += "--";
  msg3.value = { name: "张三", city: { name: "四川" } };
  msg3.value.city.name += "--"; // reactive对对象进行了深度数据劫持
  console.log(msg3.value.city);
}
</script>

<style scoped></style>
```

## 计算属性 computed

- 与 computed 配置功能一致
- 只有 getter
- 有 getter 和 setter

```vue
<template>
  <h2>App</h2>
  fistName: <input v-model="user.firstName" /><br />
  lastName: <input v-model="user.lastName" /><br />
  fullName1: <input v-model="fullName1" /><br />
  fullName2: <input v-model="fullName2" /><br />
</template>

<script setup lang="ts">
import { ref, reactive, computed } from "vue";
const user = reactive({
  firstName: "常",
  lastName: "安",
});
// 只有getter的计算属性
const fullName1 = computed(() => {
  console.log(fullName1);
  return user.firstName + "-" + user.lastName;
});
//  有getter与setter的计算属性
const fullName2 = computed({
  get() {
    console.log("fullName2 get");
    return user.firstName + "-" + user.lastName;
  },
  set(value: string) {
    console.log("fullName2 set");
    const names = value.split("-");
    user.firstName = names[0];
    user.lastName = names[1];
  },
});
</script>

<style scoped></style>
```

## 监听属性 watch

```vue
<template>
  <h2>App</h2>
  fistName: <input v-model="user.firstName" /><br />
  lastName: <input v-model="user.lastName" /><br />
  fullName3: <input v-model="fullName3" /><br />
</template>

<script setup lang="ts">
import { ref, reactive, computed, watch } from "vue";
const user = reactive({
  firstName: "常",
  lastName: "安",
});
// 只有getter的计算属性
const fullName1 = computed(() => {
  console.log(fullName1);
  return user.firstName + "-" + user.lastName;
});
const fullName3 = ref("");
/* 
  使用watch的2个特性:
    深度监视
    初始化立即执行
  */
watch(
  user,
  () => {
    fullName3.value = user.firstName + "-" + user.lastName;
  },
  {
    immediate: true, // 是否初始化立即执行一次, 默认是false
    deep: true, // 是否是深度监视, 默认是false
  }
);
/* 
  watch一个数据
    默认在数据发生改变时执行回调
  */
watch(fullName3, (value) => {
  console.log("watch");
  const names = value.split("-");
  user.firstName = names[0];
  user.lastName = names[1];
});

/* 
watch多个数据: 
  使用数组来指定
  如果是ref对象, 直接指定
  如果是reactive对象中的属性,  必须通过函数来指定
*/
watch([() => user.firstName, () => user.lastName, fullName3], (values) => {
  console.log("监视多个数据", values);
});
</script>

<style scoped></style>
```
