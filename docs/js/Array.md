# Array

## 声明数组

数组是多个变量值的集合，数组是 Array 对象的实例，所以可以像对象一样调用方法。

### 创建数组

使用对象方式创建数组

```js
console.log(new Array(1, "常安", "ChangAn")); // ['1','常安'，'ChangAn']
```

使用字面量创建是推荐的简单作法

```js
const array = ["常安", "ChangAn"];
console.log(array); //["常安","ChangAn"]
```

多维数组定义

```js
const array = [["常安"], ["ChangAn"]];
console.log(array[1][0]); //ChangAn
```

数组是引用类型可以使用 const 声明并修改它的值

```js
const array = ["常安", "ChangAn"];
array.push("长安");
console.log(array); // ['常安', 'ChangAn', '长安']
```

使用原型的 length 属性可以获取数组元素数量

```js
let ca = ["常安", "ChangAn"];
console.log(ca.length); //2
```

数组可以设置任何值，下面是使用索引添加数组

```js
let ca = ["常安"];
ca[1] = "ChangAn";
console.log(ca); //['常安','ChangAn']
```

下面直接设置 3 号数组，会将 1/2 索引的数组定义为空值

```js
let ca = ["常安"];
ca[3] = "ChangAn";
console.log(ca.length); //4
console.log(ca[1]); //undefined
console.log(ca[2]); //undefined
```

声明多个空元素的数组

```js
let ca = new Array(3);
console.log(ca.length); //3
console.log(ca); //[空属性 × 3]
```

### Array.of

使用 Array.of 与 new Array 不同是设置一个参数时不会创建空元素数组

```js
let ca = Array.of(3);
console.log(ca); //[3]
ca = Array.of(1, 2, 3);
console.log(ca); //[1, 2, 3]
```

### 类型检测

检测变量是否为数组类型

```js
console.log(Array.isArray([1, "常安", "ChangAn"])); //true
console.log(Array.isArray(9)); //false
```

## 类型转换

可以将数组转换为字符串也可以将其他类型转换为数组。

### 字符串

大部分数据类型都可以使用.toString() 函数转换为字符串。

```js
console.log([1, 2, 3].toString()); // 1,2,3
```

也可以使用函数 String 转换为字符串。

```js
console.log(String([1, 2, 3])); // 1,2,3
```

或使用 join 连接为字符串

```js
console.log([1, 2, 3].join("-")); //1-2-3
```

### Array.from

使用 Array.from 可将类数组转换为数组，类数组指包含 length 属性或可迭代的对象

- 第一个参数为要转换的数据，第二个参数为类似于 map 函数的回调方法

```js
let str = "常安";
console.log(Array.from(str)); //["常", "安"]
```

为对象设置 length 属性后也可以转换为数组，但要下标为数值或数值字符串

```js
let user = {
  0: "常安",
  1: 18,
  length: 2,
};
console.log(Array.from(user)); //["常安", 18]
```

DOM 元素转换为数组后来使用数组函数，第二个参数类似于 map 函数的方法，可对数组元素执行函数处理。

```js
// html
 <button message="后盾人">button</button>
 <button message="hdcms">button</button>
// js
   let btns = document.querySelectorAll('button')
     console.log(btns)    // 包含length属性
     Array.from(btns, (item) => {
         item.style.background = 'red';
     });
```

## 展开语法

使用展开语法将 NodeList 转换为数组操作

```js
// 点击隐藏案例

// style
.hide {
    display: none;
}
// html
<div>常安</div>
<div>ChangAn</div>
// js
let NodeList = document.querySelectorAll("div");
[...NodeList].map(function(div) {
    div.addEventListener("click", function() {
    this.classList.toggle("hide");
    });
});
```

### 数组合并

使用展开语法来合并数组相比 concat 要更简单，使用... 可将数组展开为多个值。

```js
let a = ["常安", "长治久安"];
let b = ["长安", ...a];
console.log(b); // ['长安', '常安', '长治久安']
```

### 函数参数

使用展示语法可以替代 arguments 来接收任意数量的参数

```js
function ca(...args) {
  console.log(args);
}
ca("长安", "常安", "长治久安"); // ['长安', '常安', '长治久安']
```

也可以用于接收部分参数

```js
function ca(state, ...args) {
  console.log(state, args);
}
ca("长安", "常安", "长治久安"); // 长安  ['常安', '长治久安']
```

### 节点转换

可以将 DOM 节点转为数组，下面例子不可以使用 filter 因为是节点列表

```html
<body>
  <button message="后盾人">button</button>
  <button message="hdcms">button</button>
</body>

<script>
  let btns = document.querySelectorAll("button");
  btns.filter((item) => {
    console.log(item); //TypeError: btns.filter is not a function
  });
</script>
```

使用展开语法后就可以使用数据方法

```html
<body>
  <div>常安</div>
  <div>长安</div>
</body>
<script>
  let divs = document.querySelectorAll("div");
  [...divs].map(function (div) {
    div.addEventListener("click", function () {
      this.classList.toggle("hide"); //添加class类
    });
  });
</script>
<style>
  .hide {
    color: red;
  }
</style>
```

学习后面章节后也可以使用原型处理

```html
<body>
  <div>常安</div>
  <div>长安</div>
  <script>
    let div = document.querySelectorAll("div");
    Array.prototype.map.call(div, (item) => {
      item.style.color = "red";
    });
  </script>
</body>
```

## 解构赋值

解构是一种更简洁的赋值特性，可以理解为分解一个数据的结构

- 建议使用 var/let/const 声明

### 基本使用

下面是基本使用语法

```js
//数组使用
let [name, city] = ["常安", "长安"];
console.log(name); //常安
```

解构赋值数组

```js
function name() {
  return ["常安", "长安"];
}
let [a, b] = name();
console.log(a); //常安
```

剩余解构指用一个变量来接收剩余参数

```js
let [a, ...b] = ["常安", "长安", "长治久安"];
console.log(b); //['长安', '长治久安']
```

如果变量已经初始化过，就要使用() 定义赋值表达式，严格模式会报错所以不建议使用

```js
let name = "常安";
[name, city] = ["长安", "长治久安"];
console.log(name); //长安

// 严格模式下
("use strict");
let name = "常安";
[name, city] = ["长安", "长治久安"];
console.log(name); //city is not defined
```

字符串解构

```js
"use strict";
const [...a] = "changan";
console.log(a); //Array(7)['c', 'h', 'a', 'n', 'g', 'a', 'n']
```

### 严格模式

非严格模式可以不使用声明指令，严格模式下必须使用声明。所以建议使用 let 等声明。

```js
"use strict";

[name, city] = ["常安", "长安"];
console.log(name); // city is not defined
```

### 简洁定义

只赋值部分变量

```js
let [, city] = ["常安", "长安"];
console.log(city); //长安
```

使用展开语法获取多个值

```js
let [name, ...arr] = ["常安", "长安", "长治久安"];
console.log(name, arr); //常安 (2) ['长安', '长治久安']
```

### 默认值

为变量设置默认值

```js
let [name, city = "长安"] = ["常安"];
console.log(city); //长安
```

### 函数参数

数组参数的使用

```js
function name([a, b]) {
  console.log(a, b); //常安 长安
}
name(["常安", "长安"]);
```

## 数组元素的增删改

### 基本使用

使用从 0 开始的索引来改变数组

```js
let arr = [1, "常安", "长安"];
arr[1] = "长治久安";
console.log(arr); // [1, '长治久安', '长安']
```

使用 length 向数组追加元素

```js
let arr = [1, "常安", "长安"];
arr[arr.length] = "长治久安";
console.log(arr); // [1, '常安', '长安', '长治久安']
```

### 扩展语法

使用扩展语法批量添加元素

```js
let arr = ["常安", "长安"];
let prove = ["长治久安"];
prove.push(...arr);
console.log(prove); //['长治久安', '常安', '长安']
```

### push

压入元素，直接改变原数组，返回值为数组元素数量

```js
let arr = ["常安", "长安"];
console.log(arr.push("长治久安", "常来长安")); //4
console.log(arr); //['常安', '长安', '长治久安', '常来长安']
```

根据区间创建新数组

```js
function rangeArray(begin, end) {
  const array = [];
  for (let i = begin; i <= end; i++) {
    array.push(i);
  }
  return array;
}
console.log(rangeArray(1, 6)); //[1, 2, 3, 4, 5, 6]
```

### pop

从末尾弹出元素，直接改变原数组，返回值为弹出的元素

```js
let arr = ["常安", "长安"];
console.log(arr.pop()); //长安
console.log(arr); //['常安']
```

### shift

从数组前面取出一个元素 改变原数组

```js
let arr = ["常安", "长安"];
console.log(arr.shift()); //常安
console.log(arr); //['长安']
```

### unshift

从数组前面添加元素 返回值为数组元素数量 改变原数组

```js
let arr = ["常安", "长安"];
console.log(arr.unshift("长治久安", "常来长安")); //4
console.log(arr); //['长治久安', '常来长安', '常安', '长安']
```

### fill

使用 fill 填充数组元素

```js
console.log(Array(4).fill("常安")); //["常安", "常安", "常安", "常安"]
```

指定填充位置

```js
// 语法： fill('填充内容','开始索引'，'结束索引')
console.log([1, 2, 3, 4].fill("长安", 1, 2)); //[1, '长安', 3, 4]
```

### slice

使用 slice 方法从数组中截取部分元素组合成新数组（并不会改变原数组），不传第二个参数时截取到数组的最后元素。 返回截取部分组成的新数组

```js
// 语法 arr.slice(开始截取的数组索引, 结束截取的数组索引)
let arr = [0, 1, 2, 3, 4, 5, 6];
console.log(arr.slice(1, 3)); // [1,2]
console.log(arr.slice(1)); //[1, 2, 3, 4, 5, 6]
```

不设置参数是为获取所有元素

```js
let arr = [0, 1, 2, 3, 4, 5, 6];
console.log(arr.slice()); //[0, 1, 2, 3, 4, 5, 6]
```

### splice

使用 splice 方法可以添加、删除、替换数组中的元素，会对原数组进行改变，返回值为删除的元素。 删除数组元素第一个参数为从哪开始删除，第二个参数为删除的数量

```js
let arr = [0, 1, 2, 3, 4, 5, 6];
console.log(arr.splice(0, 3)); //返回删除的元素 [0, 1, 2]
console.log(arr); //删除数据后的原数组 [3, 4, 5, 6]
```

通过修改 length 删除最后一个元素

```js
let arr = ["常安", "长安"];
arr.length = arr.length - 1;
console.log(arr); // ['常安']
```

通过指定第三个参数来设置在删除位置添加的元素

```js
let arr = [0, 1, 2, 3, 4, 5, 6];
console.log(arr.splice(1, 3, "常安", "长安")); //[1, 2, 3]
console.log(arr); //[0, "常安", "长安", 4, 5, 6]
```

向末尾添加元素

```js
let arr = [0, 1, 2, 3, 4, 5, 6];
console.log(arr.splice(arr.length, 0, "常安", "长安")); //[]
console.log(arr); // [0, 1, 2, 3, 4, 5, 6, "常安", "长安"]
```

向数组前添加元素

```js
let arr = [0, 1, 2, 3, 4, 5, 6];
console.log(arr.splice(0, 0, "常安", "长安")); //[]
console.log(arr); //["常安", "长安", 0, 1, 2, 3, 4, 5, 6]
```

数组元素位置调整函数

```js
function move(array, before, to) {
  if (before < 0 || to >= array.length) {
    console.error("指定位置错误");
    return;
  }
  const newArray = [...array];
  const elem = newArray.splice(before, 1);
  newArray.splice(to, 0, ...elem);
  return newArray;
}
const array = [1, 2, 3, 4];
console.log(move(array, 0, 3)); //[2, 3, 4, 1]
```

### 清空数组

将数组值修改为[]可以清空数组，如果有多个引用时数组在内存中存在被其他变量引用 只清空该变量 其他变量不清空。

```js
let user = [{ name: "常安" }, { city: "长安" }];
let users = user;
user = [];
console.log(user); // []
console.log(users); // [{ name: "常安" }, { city: "长安" }]
```

将数组 length 设置为 0 也可以清空数组

```js
let user = [{ name: "常安" }, { name: "长安" }];
user.length = 0;
console.log(user); //[]
```

使用 splice 方法删除所有数组元素

```js
let user = [{ name: "常安" }, { name: "长安" }];
user.splice(0, user.length);
console.log(user); //[]
```

使用 pop/shift 删除所有元素，来清空数组

```js
// 使用pop
let user = [{ name: "常安" }, { city: "长安" }];
while (user.pop()) {}
console.log(user); // []
// 使用shift
let user = [{ name: "常安" }, { city: "长安" }];
while (user.shift()) {}
console.log(user); // []
```

## 合并拆分

### join

使用 join 将数组的元素以特定字符连接成字符串

```js
let arr = ["常安", "长安", "常来长安"];
console.log(arr.join("-")); //常安-长安-常来长安 使用join可以指定转换的连接方式
```

### split

split 方法用于将字符串分割成数组，类似 join 方法的反函数,以字符串中的某一特定字符将字符串分隔成数组。

```js
let price = "常安-长安-常来长安";
console.log(price.split("-")); //['常安', '长安', '常来长安']
```

### concat

concat 方法用于连接两个或多个数组，元素是值类型的是复制操作，如果是引用类型还是指向同一对象

```js
let array = ["常安", "长安"];
let sm = ["长治久安", "常来长安"];
let bz = ["大唐不夜城", "大雁塔"];
console.log(array.concat(sm, bz)); //['常安', '长安', '长治久安', '常来长安', '大唐不夜城', '大雁塔']
```

也可以使用扩展语法实现连接

```js
let array = ["常安", "长安"];
let sm = ["长治久安", "常来长安"];
let bz = ["大唐不夜城", "大雁塔"];
console.log([...array, ...sm, ...bz]); //['常安', '长安', '长治久安', '常来长安', '大唐不夜城', '大雁塔']
```

### copyWithin

使用 copyWithin 从数组中复制一部分到同数组中的另外位置。
语法说明

```js
array.copyWithin(target, start, end);
```

参数说明

| 参数   |                                  描述                                  |
| :----- | :--------------------------------------------------------------------: |
| target |                     必需。复制到指定目标索引位置。                     |
| start  |                       可选。元素复制的起始位置。                       |
| end    | 可选。停止复制的索引位置 (默认为 array.length)。如果为负值，表示倒数。 |

```js
const arr = [1, 2, 3, 4];
console.log(arr.copyWithin(2, 0, 2)); //[1, 2, 1, 2]
```

## 查找元素

数组包含多种查找的函数，需要把这些函数掌握清楚，然后根据不同场景选择合适的函数。

### indexOf

使用 indexOf 从前向后查找元素出现的位置，找到返回第一次出现该元素的位置索引，如果找不到返回 -1。

```js
let arr = [7, 3, 2, 8, 2, 6];
console.log(arr.indexOf(2)); // 2 从前面查找2 第一次出现的位置
```

如下面代码一下，使用 indexOf 查找字符串将找不到，因为 indexOf 类似于===是严格类型约束。

```js
let arr = [7, 3, 2, "8", 2, 6];
console.log(arr.indexOf(8)); // -1
```

第二个参数用于指定查找开始位置

```js
let arr = [7, 3, 2, 8, 2, 6];
//从索引为3的位置开始查找
console.log(arr.indexOf(2, 3)); //4
```

### lastIndexOf

使用 lastIndexOf 从后向前查找元素出现的位置，找到返回第一次出现该元素的位置索引，如果找不到返回 -1。

```js
let arr = [7, 3, 2, 8, 2, 6];
console.log(arr.lastIndexOf(2)); // 4 从后查找第一次2出现的位置
```

第二个参数用于指定查找开始位置

```js
let arr = [7, 3, 2, 8, 2, 6];
//从索引为3的位置开始向左查找
console.log(arr.lastIndexOf(2, 3)); // 2 从后查找第一次2出现的位置

//从前往后索引为2的位置开始向左查找
console.log(arr.lastIndexOf(2, -2));
```

### includes

使用 includes 查找字符串返回值是布尔类型更方便判断

```js
let arr = [7, 3, 2, 6];
console.log(arr.includes(6)); //true
```

我们来实现一个自已经的 includes 函数，来加深对 includes 方法的了解

```js
function includes(array, item) {
  for (const value of array) if (item === value) return true;
  return false;
}

console.log(includes([1, 2, 3, 4], 3)); //true
```

### find

find 方法找到后会把值返回出来 如果找不到返回值为 undefined 返回第一次找到的值，不继续查找

```js
let arr = ["常安", "长安", "长治久安"];

let find = arr.find(function (item) {
  return item == "长安";
});

console.log(find); //长安
```

使用 includes 等不能查找引用类型，因为它们的内存地址是不相等的

```js
const user = [{ name: "常安" }, { name: "长安" }, { name: "张三" }];
const find = user.includes({ name: "常安" });
console.log(find); //false
```

find 可以方便的查找引用类型

```js
const user = [{ name: "常安" }, { name: "长安" }, { name: "张三" }];
const find = user.find((user) => (user.name = "张三"));
console.log(find); //{name: '张三'}
```

### findIndex

findIndex 与 find 的区别是返回索引值，参数也是 : 当前值，索引，操作数组。

- 查找不到时返回 -1

```js
let arr = [7, 3, 2, "8", 2, 6];

console.log(
  arr.findIndex(function (v) {
    return v == 8;
  })
); //3
```

### find 原理

下面使用自定义函数

```js
let arr = [1, 2, 3, 4, 5];
function find(array, callback) {
  for (const value of array) {
    if (callback(value) === true) return value;
  }
  return undefined;
}
let res = find(arr, function (item) {
  return item == 23;
});
console.log(res);
```

下面添加原型方法实现

```js
Array.prototype.findValue = function (callback) {
  for (const value of this) {
    if (callback(value) === true) return value;
  }
  return undefined;
};

let re = arr.findValue(function (item) {
  return item == 2;
});
console.log(re);
```

## 数组排序

### reverse

反转数组顺序

```js
let arr = [1, 4, 2, 9];
console.log(arr.reverse()); //[9, 2, 4, 1]
```

### sort

sort 每次使用两个值进行比较 Array.sort((a,b)=>a-b)
默认从小到大排序数组元素

```js
let arr = [1, 4, 2, 9];
console.log(arr.sort()); //[1, 2, 4, 9]
```

使用排序函数从大到小排序，参数一与参数二比较，返回正数为降序 负数为升序

```js
let arr = [1, 4, 2, 9];

console.log(
  arr.sort(function (v1, v2) {
    return v2 - v1;
  })
); //[9, 4, 2, 1]
```

下面是按旅游人数由高到低排序

```js
let tourism = [
  { city: "长安", peoples: 78 },
  { city: "洛阳", peoples: 12 },
  { city: "云南", peoples: 99 },
];
let sortLessons = tourism.sort((v1, v2) => v2.peoples - v1.peoples);
console.log(sortLessons);
```

### 排序原理

```js
let arr = [1, 5, 3, 9, 7];
function sort(array, callback) {
  for (const n in array) {
    for (const m in array) {
      if (callback(array[n], array[m]) < 0) {
        let temp = array[n];
        array[n] = array[m];
        array[m] = temp;
      }
    }
  }
  return array;
}
arr = sort(arr, function (a, b) {
  return a - b;
});
console.table(arr);
```

## 循环遍历

### for

根据数组长度结合 for 循环来遍历数组

```js
let tourism = [
  { city: "长安", peoples: 78 },
  { city: "洛阳", peoples: 12 },
  { city: "云南", peoples: 99 },
];

for (let i = 0; i < tourism.length; i++) {
  tourism[i] = `常安去: ${tourism[i].city}`;
}
console.log(tourism); // ['常安去: 长安', '常安去: 洛阳', '常安去: 云南']
```

### forEach

forEach 使函数作用在每个数组元素上，但是没有返回值。下面例子是截取标签的五个字符。

```js
let tourism = [
  { city: "长安", peoples: 78 },
  { city: "洛阳", peoples: 12 },
  { city: "云南", peoples: 99 },
];

tourism.forEach((item, index, array) => {
  item.city = item.city.substr(0, 1);
});
console.log(tourism); //   [{ city: "长", peoples: 78 },{ city: "洛", peoples: 12 },{ city: "云", peoples: 99 },];
```

### #for/in

遍历时的 key 值为数组的索引

```js
let tourism = [
  { city: "长安", peoples: 78 },
  { city: "洛阳", peoples: 12 },
  { city: "云南", peoples: 99 },
];

for (const key in tourism) {
  console.log(`城市: ${tourism[key].city}`); // 城市: 长安  城市: 洛阳  城市: 云南
}
```

### for/of

与 for/in 不同的是 for/of 每次循环取其中的值而不是索引。

```js
let tourism = [
  { city: "长安", peoples: 78 },
  { city: "洛阳", peoples: 12 },
  { city: "云南", peoples: 99 },
];

for (const item of tourism) {
  console.log(
    `城市: ${item.city}  旅游人数: ${
      item.peoples > 50 ? "人数太多了" : "人数合适"
    }`
  );
} // 城市: 长安  旅游人数: 人数太多了  城市: 洛阳  旅游人数: 人数合适  城市: 云南  旅游人数: 人数太多了
```

使用数组的迭代对象遍历获取索引与值（有关迭代器知识后面章节会讲到）

```js
const name = ["常安", "长安"];
const iterator = name.entries();
console.log(iterator.next()); //value:{0:0,1:'常安'}
console.log(iterator.next()); //value:{0:1,1:'长安'}
```

这样就可以使用解构特性与 for/of 遍历并获取索引与值了

```js
const name = ["常安", "长安"];

for (const [key, value] of name.entries()) {
  console.log(key, value); // 0 常安 1 长安
}
```

取数组中的最大值

```js
function arrayMax(array) {
  let max = array[0];
  for (const elem of array) {
    max = max > elem ? max : elem;
  }
  return max;
}

console.log(arrayMax([1, 3, 2, 9])); // 9
```

## 迭代器方法

数组中可以使用多种迭代器方法，迭代器后面章节会详解。

### keys

通过迭代对象获取索引

```js
const name = ["常安", "长安"];
const keys = name.keys();
console.log(keys.next()); // value: 0
console.log(keys.next()); // value: 1
```

获取数组所有索引

```js
const arr = ["a", "b", "c", "d"];

for (const key of arr.keys()) {
  console.log(key); // 0 1 2 3
}
```

### values

通过迭代对象获取值

```js
const name = ["长安", "常安"];
const values = name.values();
console.log(values.next()); //value:长安
console.log(values.next()); //value : 常安
console.log(values.next()); //value : undefined
```

获取数组的所有值

```js
const arr = ["a", "b", "c", "d"];
for (const value of arr.values()) {
  console.log(value); // a b c d
}
```

### entries

返回数组所有键值对，下面使用解构语法循环

```js
const arr = ["a", "b", "c", "d"];
for (const [key, value] of arr.entries()) {
  console.log(key, value); // 0 'a' 1 'b' 2 'c' 3 'd'
}
```

解构获取内容（对象章节会详细讲解）

```js
const name = ["常安", "长安"];
const iterator = name.entries();
let {
  done,
  value: [k, v],
} = iterator.next();
console.log(v); //常安
```

## 扩展方法

### every

every 用于递归的检测元素，要所有元素操作都要返回真结果才为真。

查看旅游地的人数是否都大于 50

```js
let tourism = [
  { city: "长安", peoples: 78 },
  { city: "洛阳", peoples: 12 },
  { city: "云南", peoples: 99 },
];
const resust = tourism.every((tourism) => tourism.peoples >= 50);
console.log(resust); // false
```

标题的关键词检查

```js
let words = ["常安", "长安", "五一"];
let title = "常安五一去长安";

let state = words.every(function (item, index, array) {
  return title.indexOf(item) >= 0;
});

if (state == false) console.log("标题必须包含所有关键词");
```

### some

使用 some 函数可以递归的检测元素，如果有一个返回 true，表达式结果就是真。第一个参数为元素，第二个参数为索引，第三个参数为原数组。

下面是使用 some 检测规则关键词的示例，如果匹配到一个词就提示违规。

```js
let words = ["常安", "长安", "五一"];
let title = "常安去洛阳";

let state = words.some(function (item, index, array) {
  return title.indexOf(item) >= 0;
});

if (state) console.log("标题含有违规关键词");
```

### filter

使用 filter 可以过滤数据中元素，下面是获取旅游人数大于 50 的城市。

```js
let tourism = [
  { city: "长安", peoples: 78 },
  { city: "洛阳", peoples: 12 },
  { city: "云南", peoples: 99 },
];
let qualified = tourism.filter(function (item, index, array) {
  if (item.peoples >= 50) {
    return true;
  }
});
console.log(qualified); // {city: '长安', peoples: 78}  {city: '云南', peoples: 99}
```

写一个过滤元素的方法来加深些技术

```js
function except(array, excepts) {
  const newArray = [];
  for (const elem of array) if (!excepts.includes(elem)) newArray.push(elem);
  return newArray;
}

const array = [1, 2, 3, 4];
console.log(except(array, [2, 3])); //[1,4]
```

### map

使用 map 映射可以在数组的所有元素上应用函数，用于映射出新的值
获取数组所有标题组合的新数组

```js
let tourism = [
  { city: "长安", peoples: 78 },
  { city: "洛阳", peoples: 12 },
  { city: "云南", peoples: 99 },
];

console.log(tourism.map((item) => item.city)); //['长安', '洛阳', '云南']
```

为所有城市添加上 常安五一去

```js
let tourism = [
  { city: "长安", peoples: 78 },
  { city: "洛阳", peoples: 12 },
  { city: "云南", peoples: 99 },
];
tourism = tourism.map(function (item, index, array) {
  item.city = `常安五一去${item.city}`;
  return item;
});
console.log(tourism);
```

### reduce

使用 reduce 与 reduceRight 函数可以迭代数组的所有元素，reduce 从前开始 reduceRight 从后面开始。下面通过函数计算旅游人数总和。

reduce参数
第一个参数是执行函数，第二个参数为初始值
- 传入第二个参数时将所有元素循环一遍
- 不传第二个参数时从第二个元素开始循环


  执行函数参数说明如下

  | 参数  |            说明            |
  | :---- | :------------------------: |
  | prev  | 上次调用回调函数返回的结果 |
  | cur   |        当前的元素值        |
  | index |         当前的索引         |
  | array |           原数组           |

统计元素出现的次数

```js
function countArrayELem(array, elem) {
  return array.reduce((total, cur) => (total += cur == elem ? 1 : 0), 0);
}

let numbers = [1, 2, 3, 1, 5, 1, 6];
console.log(countArrayELem(numbers, 1)); //3
```

取数组中的最大值

```js
function arrayMax(array) {
  return array.reduce((max, elem) => (max > elem ? max : elem), array[0]);
}

console.log(arrayMax([1, 3, 2, 9]));
```

取价格最高的商品

```js
let cart = [
  { name: "iphone", price: 12000 },
  { name: "imac", price: 25000 },
  { name: "ipad", price: 3600 },
];

function maxPrice(array) {
  return array.reduce(
    (goods, elem) => (goods.price > elem.price ? goods : elem),
    array[0]
  );
}
console.log(maxPrice(cart));
```

使用 reduce 实现数组去重

```js
let arr = [1, 2, 6, 2, 1];
let filterArr = arr.reduce((pre, cur, index, array) => {
  if (pre.includes(cur) === false) {
    pre = [...pre, cur];
  }
  return pre;
}, []);
console.log(filterArr); // [1,2,6]
```

## Array完 欢迎补充 