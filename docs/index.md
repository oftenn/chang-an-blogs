---
layout: home
hero:
  name: 前端开发
  text: 以梦为马，不负韶华
  tagline: 从入门到进阶，你想学的这里都有
  image:
    src: /logo.jpg   
    alt: image     
  actions:
    - theme: brand
      text: 进入学习
      link: /guide/start
    - theme: alt
      text: 在 Gitee 上查看
      link: https://gitee.com/oftenn/chang-an-blogs
features:
- icon: ⚡️
  title: vue2
  details: 一套用于构建用户界面的渐进式框架
  link: /guide/start
  linkText: 了解更多
- icon: ⚡️
  title: js
  details: js基础
  link: /js/js
  linkText: 了解更多
- icon: ⚡️
  title: vue3
  details: vue3基础
  link: /vue3/vue3
  linkText: 了解更多
- icon: ⚡️
  title: ts
  details: ts基础
  link: /ts/ts
  linkText: 了解更多
---
<!-- <script setup>
import home from './.vitepress/components/FreeStyle.vue'
</script>
<home></FreeStyle> -->

<!-- hero 主页顶部
   name 是文档标题
text 是类似于文档副标题
tagline 是文档标语
  
  src: /logo.png
    alt: 网页的logo图标
  
  线上访问地址
  http://oftenn.gitee.io/chang-an-blogs
  -->
