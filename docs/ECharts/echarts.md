# ECharts

### ECharts 基本使用

```js
<!DOCTYPE html>
<html>
  <head>
    <script src="https://cdn.jsdelivr.net/npm/echarts@4.7.0/dist/echarts.min.js"></script>
    <style>
      #chart {
        width: 800px;
        height: 400px;
      }
    </style>
  </head>
  <body>
    <div id="chart"></div>
    <script>
      const chartDom = document.getElementById('chart')
      const chart = echarts.init(chartDom)
      chart.setOption({
        title: {
          text: '快速入门ECharts开发'
        },
        xAxis: {
          data: ['食品', '数码', '服饰', '箱包']
        },
        yAxis: {},
        series: {
          type: 'bar',
          data: [100, 120, 90, 150]
        }
      })
    </script>
  </body>
</html>
```

### ECharts 使用流程

> - 引入 js 库
> - 编写渲染容器 DOM，添加 width 和 height 样式属性
> - 获取渲染 DOM 对象
> - 初始化 ECharts 对象
> - 编写 option 参数
> - 调用 setOption 完成渲染

### init 的参数

> 1. 参数一 chartDom 需要加载的 Dom
> 2. 参数二 vintage 加载的自定义主题
> 3. 参数三 {renderer:'svg'} 改为 svg 渲染
>    如下

```js
const chartDom = document.getElementById("chart");
const chart = echarts.init(chartDom, "vintage", { renderer: "svg" });
```

### ECharts 的系列

系列（series）是指：一组数值映射成对应的图

**_多系列开发案例_**

```js
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/echarts@4.7.0/dist/echarts.min.js"></script>
    <style>
      #chart {
        width: 800px;
        height: 400px;
      }
    </style>
  </head>
  <body>
    <div id="chart"></div>
    <script>
      const chartDom = document.getElementById('chart')
      const chart = echarts.init(chartDom)
      const option = {
        xAxis: {
          data: ['一季度', '二季度', '三季度', '四季度']
        },
        yAxis: {},
        series: [{
          type: 'pie',
          center: ['65%', 60],
          radius: 35,
          data: [{
            name: '分类1', value: 50
          }, {
            name: '分类2', value: 60
          }, {
            name: '分类3', value: 55
          }, {
            name: '分类4', value: 70
          }]
        }, {
          type: 'line',
          data: [100, 112, 96, 123]
        }, {
          type: 'bar',
          data: [79, 81, 88, 72]
        }]
      }
      chart.setOption(option)
    </script>
  </body>
</html>
```

### ECharts 4.0 新特性：dataset

ECharts 4 开始支持了 数据集（dataset）组件用于单独的数据集声明，从而数据可以单独管理，被多个组件复用，并且可以自由指定数据到视觉的映射。这一特性能将逻辑和数据分离，带来更好的复用，并易于理解。
**_ 案例：dataset 移植 _**

```js
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/echarts@4.7.0/dist/echarts.min.js"></script>
    <style>
      #chart {
        width: 800px;
        height: 400px;
      }
    </style>
  </head>
  <body>
    <div id="chart"></div>
    <script>
      const chartDom = document.getElementById('chart')
      const chart = echarts.init(chartDom)
      const option = {
        xAxis: {
          type: 'category'
        },
        yAxis: {},
        //代替数据源
        dataset: {
          source: [
            ['一季度', 79, 100, '分类1', 50],
            ['二季度', 81, 112, '分类2', 60],
            ['三季度', 88, 96, '分类3', 55],
            ['四季度', 72, 123, '分类4', 70],
          ]
        },
        series: [{
          type: 'pie',
          center: ['65%', 60],
          radius: 35,
          // 代替饼图data
          encode: { itemName: 3, value: 4 }
        }, {
          type: 'line',
          // 代替折线图data
          encode: { x: 0, y: 2 }
        }, {
            //代替柱状图data
          type: 'bar',
          encode: { x: 0, y: 1 }
        }]
      }
      chart.setOption(option)
    </script>
  </body>
</html>
```

### ECharts 组件

ECharts 中除了绘图之外其他部分，都可抽象为 「组件」。例如，ECharts 中至少有这些组件：xAxis（直角坐标系 X 轴）、yAxis（直角坐标系 Y 轴）、grid（直角坐标系底板）、angleAxis（极坐标系角度轴）...

```js
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/echarts@4.7.0/dist/echarts.min.js"></script>
    <style>
      #chart {
        width: 800px;
        height: 400px;
      }
    </style>
  </head>
  <body>
    <div id="chart"></div>
    <script>
      const chartDom = document.getElementById('chart')
      const chart = echarts.init(chartDom)
      const option = {
        // 标题组件
        title: {
          text: '数据可视化',
          // 副标题
          subtext: '数据可视化'
        },
        xAxis: {
          type: 'category'
        },
        yAxis: {},
        // 图例组件
        legend: {
          data: [{
            name: '分类',
            // 强制设置图形为圆。
            icon: 'circle',
            // 设置文本为红色
            textStyle: {
              color: 'red'
            }
          }, '折线图', '柱状图'],
          // 图例位置
          left: 100
        },
        // 保存图片等组件
        toolbox: {
          feature: {
            dataZoom: {
              yAxisIndex: 'none'
            },
            restore: {},
            saveAsImage: {}
          }
        },
        // 底部移动组件
        dataZoom: [{
          show: true,
          start: 0,
          end: 30
        }],
        dataset: {
          source: [
            ['一季度', 79, 100, '分类1', 50],
            ['二季度', 81, 112, '分类2', 60],
            ['三季度', 88, 96, '分类3', 55],
            ['四季度', 72, 123, '分类4', 70],
          ]
        },
        grid: [{
          left: 50,
          top: 70
        }],
        series: [{
          // 跟图例组件相对应
          name: '分类',
          type: 'pie',
          center: ['65%', 60],
          radius: 35,
          encode: { itemName: 3, value: 4 }
        }, {
          name: '折线图',
          type: 'line',
          encode: { x: 0, y: 2 }
        }, {
          name: '柱状图',
          type: 'bar',
          encode: { x: 0, y: 1 }
        }]
      }
      chart.setOption(option)
    </script>
  </body>
</html>
```

### ECharts 定位

大多数组件都提供了定位属性，我们可以采用类似 CSS absolute 的定位属性来控制组件的位置，下面这个案例可以通过修改 grid 组件定位来控制图表的位置

### ECharts 坐标系

很多系列，例如 line（折线图）、bar（柱状图）、scatter（散点图）、heatmap（热力图）等等，需要运行在 “坐标系” 上。坐标系用于布局这些图，以及显示数据的刻度等等。例如 ECharts 中至少支持这些坐标系：直角坐标系、极坐标系、地理坐标系（GEO）、单轴坐标系、日历坐标系 等。其他一些系列，例如 pie（饼图）、tree（树图）等等，并不依赖坐标系，能独立存在。还有一些图，例如 graph（关系图）等，既能独立存在，也能布局在坐标系中，依据用户的设定而来。

一个坐标系，可能由多个组件协作而成。我们以最常见的直角坐标系来举例。直角坐标系中，包括有 xAxis（直角坐标系 X 轴）、yAxis（直角坐标系 Y 轴）、grid（直角坐标系底板）三种组件。xAxis、yAxis 被 grid 自动引用并组织起来，共同工作。

### ECharts 双坐标系

两个 yAxis，共享了一个 xAxis。两个 series，也共享了这个 xAxis，但是分别使用不同的 yAxis，使用 yAxisIndex 来指定它自己使用的是哪个 yAxis
**_双坐标系案例 _**

```js
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/echarts@4.7.0/dist/echarts.min.js"></script>
    <style>
      #chart {
        width: 800px;
        height: 400px;
      }
    </style>
  </head>
  <body>
    <div id="chart"></div>
    <script>
      const chartDom = document.getElementById('chart')
      const chart = echarts.init(chartDom)
      const option = {
        legend: {},
        tooltip: {},
        xAxis: {
          type: 'category'
        },
        yAxis: [{
          min: 0,
          max: 100
        }, {
          min: 0,
          max: 100
        }],
        dataset: {
          source: [
            ['product', '2012', '2013', '2014', '2015'],
            ['Matcha Latte', 41.1, 30.4, 65.1, 53.3],
            ['Milk Tea', 86.5, 92.1, 85.7, 83.1]
          ]
        },
        series: [
            //yAxisIndex 来指定yAxis坐标
          { type: 'bar', seriesLayoutBy: 'row', yAxisIndex: 0 },
          { type: 'line', seriesLayoutBy: 'row', yAxisIndex: 1 }
        ]
      }
      chart.setOption(option)
    </script>
  </body>
</html>
```

### ECharts 多坐标系

一个 ECharts 实例中，有多个 grid，每个 grid 分别有 xAxis、yAxis，他们使用 xAxisIndex、yAxisIndex、gridIndex 来指定引用关系：
*** 多坐标系案例 ***

```js
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/echarts@4.7.0/dist/echarts.min.js"></script>
    <style>
      #chart {
        width: 800px;
        height: 400px;
      }
    </style>
  </head>
  <body>
    <div id="chart"></div>
    <script>
      const chartDom = document.getElementById('chart')
      const chart = echarts.init(chartDom)
      const option = {
        legend: {},
        tooltip: {},
        xAxis: [{
          type: 'category',
          gridIndex: 0
        }, {
          type: 'category',
          gridIndex: 1
        }],
        yAxis: [{
          gridIndex: 0
        }, {
          gridIndex: 1
        }],
        dataset: {
          source: [
            ['product', '2012', '2013', '2014', '2015'],
            ['Matcha Latte', 41.1, 30.4, 65.1, 53.3],
            ['Milk Tea', 86.5, 92.1, 85.7, 83.1],
            ['Cheese Cocoa', 24.1, 67.2, 79.5, 86.4]
          ]
        },
        grid: [{
          bottom: '55%'
        }, {
          top: '55%'
        }],
        series: [
          // 这几个系列会在第一个直角坐标系中，每个系列对应到 dataset 的每一行。
          { type: 'bar', seriesLayoutBy: 'row' },
          { type: 'bar', seriesLayoutBy: 'row' },
          { type: 'bar', seriesLayoutBy: 'row' },
          // 这几个系列会在第二个直角坐标系中，每个系列对应到 dataset 的每一列。
          { type: 'bar', xAxisIndex: 1, yAxisIndex: 1 },
          { type: 'bar', xAxisIndex: 1, yAxisIndex: 1 },
          { type: 'bar', xAxisIndex: 1, yAxisIndex: 1 },
          { type: 'bar', xAxisIndex: 1, yAxisIndex: 1 }
        ]
      }
      chart.setOption(option)
    </script>
  </body>
</html>
```

